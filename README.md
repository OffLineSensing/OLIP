# OLIP: Off-Line Interactor Printing

**O**ff-**L**ine **I**nteractor **P**rinting is used to specify and generate printer-ready 3D models for an off-line sensor.
Such sensors are passive 3D-printed objects that detect one-time interactions, such as accelerating or flipping, but neither require active electronics nor power at the time of the interaction. They memorize a pre-defined interaction via an embedded structure filled with a conductive medium (e.g., a liquid). Whether a sensor was exposed to the interaction can be read-out via a capacitive touchscreen and OLIR.

More information on off-line sensors can be found [here](https://www.tk.informatik.tu-darmstadt.de/de/research/tangible-interaction/off-line-sensing/).

![OLIP Screenshot](misc/olip.png)


-----



## Installing OLIP
See [Releases](../../releases)

#### Dependencies
Please install the [JRE](http://www.oracle.com/technetwork/java/javase/downloads) (at least version 8) and [OpenSCAD](https://www.openscad.org).





-----


## Compiling and Building OLIP

This project was build with the IntelliJ Idea IDE. For compiling and building we recommend to use it.

#### Compiling

To compile the project, clone or download the zip file from the git repository and open the project folder from IntelliJ Idea. The main class is located under `src/ui/Main.java`. 

#### Building a Release

To set up the executable jar artifact, go to Project Structure or push `Ctrl + Alt + Shift + S`. Go to Artifacts and push the green plus symbol. There choose `Jar -> From modules with dependencies...`. Leave the field for Main Class blank as it produces errors. Make sure the Jar files from libraries settings is set to `extract to the target JAR`. Click OK on both windows.

To build the executable jar artifact, go to `Build -> Build Artifacts... -> OLIP.jar -> Build`. After building the jar, it will be located under `out/artifacts/OLIP_jar/OLIP.jar`



-----


## Contributing
In case of any questions, please contact us via [mail](mailto:schmitz@tk.tu-darmstadt.de) or create an [issue](../../issues/).