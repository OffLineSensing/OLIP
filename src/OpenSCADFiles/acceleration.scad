//This object changes its state if a specific acceleration is reached.
//The necessary acceleration is dependend on the wall height.
//Water will be filled into the bigger chamber and will swap over that wall into the smaller chamber where it can be detected.


//Parting wall height
wallHeight = 18;
//Render conductive or non-conductive parts
conductive = false;
//Render the cross section or complete object
crossSection = false;
//Add a hole to fill the object after finishing printing
fillLater = false;
//Diameter of the conductive part that will be on the touch screen
sizeScreen=4.0;
//Diameter of the conductive part that will be connected to the finger
sizeFinger=4.0;

//The main block where everything else gets cut out
module baseBlock() {
    cube([40,20,30], center=true);
}

//Adding 2 half-cylinder on the sides for easier recognition which object this is
module recognitionAddition() {
    difference (){
        union() {
            color("green")
            translate([0,-10,5])
            cylinder(r=3,h=40, $fa=1, $fs=0.5, center=true);
            
            color("green")
            translate([0,10,5])
            cylinder(r=3,h=40, $fa=1, $fs=0.5, center=true);
        }
        baseBlock();
        roofAddition();
   }
}

//The smaller chamber that contains the conductive parts
module conductiveChamber() {
    color("blue")
    translate([-12,0,2])
    cube([8,16,26], center=true);
}

//The bigger chamber that contains the water
module initialWaterChamber() {
    color("blue")
    translate([6,0,2])
    cube([24,16,26], center=true);
}

//The wall where the water will swap over
module partitionWall() {
    color("blue")
    translate([-6,0,2+(wallHeight/2)])
    cube([10,16,26-wallHeight], center=true);
}

//Addition to the main block, added later. TODO remove, add to baseBlock
module roofAddition() {
    translate([0,0,20])
    cube([40,20,10], center=true);
}

//The roof in a 45 degree angle for better printing
module roofCutout() {
    difference() {
        color("blue")
        translate([1,0,15])
        rotate([45,0,0])
        cube([34,11.3,11.3], center=true);
        translate([1,0,2])
        cube([34,20,20],center=true);
    }
}

//The hole through which the water can be filled in later
module fillInHole() {
    color("blue")
    translate([7,0,23])
    cylinder(r=1,h=4, $fa=1, $fs=0.5, center=true);
}

//Conductive bar on the wall which is connected to the finger
module condBarWall() {
    color("red")
    translate([-16.5,0,-8])
    cube([1,14,4], center=true);
}

//Conductive part on the outside where the finger has to be placed
module condFinger() {
    color("red")
    translate([-19,0,0])
    rotate([0,90,0])
    cylinder(r=sizeFinger/2,h=2, $fa=1, $fs=0.5, center=true);
}

//A conductive part inside of a wall that connects the two modules above
module condConnectionFingerWall() {
    color("red")
    translate([-17.5,0,-4])
    cube([1,6,12], center=true);
}

//Conductive bar on the floor that is connected to the touch screen
module condBarFloor() {
    color("red")
    translate([-10,0,-11.5])
    cube([4,14,1], center=true);
}

//Conductive part on the outside that connects to the touchscreen
module condFloorScreen() {
    color("red")
    translate([-10,0,-13.5])
    cylinder(r=sizeScreen/2,h=3, $fa=1, $fs=0.5, center=true);
}

//Combine all conductive parts to one module
module conductive() {
    union() {
        condBarFloor();
        condBarWall();
        condFinger();
        condConnectionFingerWall();
        condFloorScreen();
    }
}

//The complete non-conducitve object
module completeObject() {
    difference() {
       union() {
            baseBlock();
            roofAddition();
            recognitionAddition();
        }
        union() {
            conductiveChamber();
            initialWaterChamber();
            conductive();
            partitionWall();
            roofCutout();
            if (fillLater)
                fillInHole();
        }
    }
}

//Render the object depending on the input
if (crossSection) {
    difference() {
        if (conductive) {
            conductive();
        }
        else {
            completeObject();
        }
        translate([-30,-60,-30])
        cube([60,60,60]);
    }
}
else {
    if (conductive) {
        conductive();
    }
    else {
        completeObject();
    }
}