//Render conductive or non-conductive parts
conductive = false;
//Render the cross section or complete object
crossSection = true;
//Add a hole to fill the object after finishing printing
fillLater = true;
//Diameter of the conductive part that will be on the touch screen
sizeScreen=4.0;
//Diameter of the conductive part that will be connected to the finger
sizeFinger=4.0;

//Base block where everything else gets cut out
module baseBlock() {
    translate([0,0,3.5])
     cylinder(r=17,h=51, $fa=1, $fs=0.5, center=true);
}

//Cylinder cut out, outer ring
module outerRingCutOut() {
    color("blue")
    translate([0,0,-1])
    cylinder(r=14,h=30, $fa=1, $fs=0.5, center=true);
}

//The hole through which the water can be filled in later
module fillInHole() {
    color("blue")
    translate([0,0,23])
    cylinder(r=1,h=14, $fa=1, $fs=0.5, center=true);
}

//Parting wall, inner cylinder, cut out
module partingWallInner() {
    color("blue")
    cylinder(r=6.5,h=30, $fa=1, $fs=0.5, center=true);
}

//Parting wall, outer cylinder, wall thickness
module partingWallOuter() {
    cylinder(r=8,h=30, $fa=1, $fs=0.5, center=true);
}

//Combining the two to one module, complete parting wall
module partingWall() {
    translate([0,0,-1])
    difference() {
        partingWallOuter();
        partingWallInner();
    }
}

//Parting wall, ceiling
module partingWallCeiling() {
    color("blue")
    translate([0,0,14])
    difference() {
        sphere(r=8, center=true);
        sphere(r=6.5, center=true);
        translate([0,0,-5])
        cube([20,20,10], center=true);
        translate([0,0,7])
        rotate([0,0,90])
        cylinder(r=3.5,h=4, $fa=1, $fs=0.5, center=true);
    }
}

//Cut out sphere, ceiling for the whole object
module ball() {
    color("blue")
    sphere(r=14, center=true);
}

//cube that cuts away half the ball
module cutCube() {
    translate([0,0,-7.5])
    cube([30,30,15],center=true);
}

//Ceiling for the whole object
module ceiling() {
    translate([0,0,14])
    difference() {
        ball();
        cutCube();
        if (fillLater)
            fillInHole();
    }
}

//Slope to collect the water at one point
module slope() {
    difference() {
        translate([5,0,-16])
        rotate([0,-30,0])
        cylinder(r=16.5, h=20, $fa=1, $fs=0.5, center=true);
        
        partingWallInner();
    }
}

//Connects the conductive point on the touch screen with the inner conductive part 
module condConnectionHorizontal() {
    color("red")
    translate([-4.5,0,-19])
    cube([12,4,2], center=true);
}

//Conductive part on the bottom that connects to the touch screen
module condTouchscreen() {
    color("red")
    translate([0,0,-21])
    cylinder(r=2, h=2, $fa=1, $fs=0.5, center=true);
}

//Conductive part that connects the inner conductive part with the touch screen
module condConnectionVertical() {
    color("red")
    translate([-9.5,0,-16])
    cube([2,4,4], center=true);
}

//Conductive part inside for touchscreen
module condFloor() {
    intersection() {
        color("red")
        translate([-9.5,0,-13])
        cube([2,16,2], center=true);
        
        slope();
    }
}

//Conducitve part for the finger
module condFinger() {
    difference() {
        intersection() {
            color("red")
            translate([-15.5,0,-8])
            rotate([0,90,0])
            cylinder(r=2, h=4, $fa=1, $fs=0.5, center=true);
            
            baseBlock();
        }
        outerRingCutOut();
    }
}

//All conductive parts in one module
module conductive() {
    union() {
        condConnectionHorizontal();
        condTouchscreen();
        condConnectionVertical();
        condFloor();
        condFinger();
    }
}

//The complete object
module completeObject() {
    union() {
        difference() {
            baseBlock();
            union() {
                outerRingCutOut();
                if (fillLater)
                    fillInHole();
                ceiling();
                conductive();
                
            }
        }
        partingWall();
        partingWallCeiling();
        difference() {
            intersection() {
                slope();
                baseBlock();
            }
            conductive();
        }
    }
}

//Render the object depending on the input
if (crossSection) {
    difference() {
        if (conductive) {
            conductive();
        }
        else {
            completeObject();
        }
        translate([-30,-60,-30])
        cube([60,60,60]);
    }
}
else {
    if (conductive) {
        conductive();
    }
    else {
        completeObject();
    }
}