//This object changes its state when the water in the bigger chamber is freezing.
//The ice will break through the red wall, when melting water will flow through the crack into the smaller chamber.

//Render conductive or non-conductive parts
conductive = false;
//Render the cross section or complete object
crossSection = true;
//Add a hole to fill the object after finishing printing
fillLater = true;
//Diameter of the conductive part that will be on the touch screen
sizeScreen=4.0;
//Diameter of the conductive part that will be connected to the finger
sizeFinger=4.0;

//Base block where everything else gets cut out
module baseBlock() {
    translate([-7.5,0,-1])
    cube([43,20,32], center=true);
}

//Part on the outside for easier recognition which object this is
module recognitionAddition() {
    difference (){
        color("green")
        translate([0,-10,-1])
        cylinder(r=3,h=32, $fa=1, $fs=0.5, center=true);
        baseBlock();
   }
}

//chamber where the water gets filled in
module waterChamber() {
    color("blue")
    translate([0,0,-2])
    cube([25,16,16], center=true);
}

//Chamber where water will be caught
module frozenChamber() {
    color("blue")
    translate([-19,0,-4])
    cube([12,16,12], center=true);
}

//Cheiling for the chamber with the caught water
module ceilingFrozen() {
    color("blue")
    difference() {
        translate([-13,0,2])
        rotate([0,45,0])
        cube([17,16,17], center=true);
        
        translate([-3,0,2])
        cube([20,20,40], center=true);
        
        translate([-13,0,-8])
        cube([40,20,20], center=true);
        
    }
}

//Ceiling for the water chamber
module ceiling() {
     color("blue")
    translate([0,0,6])
    rotate([45,0,0])
    cube([25,11.3,11.3], center=true);
}

//Hole to fill the object after printing
module fillInHole() {
     color("blue")
    translate([0,0,14])
    cylinder(r=1,h=2, $fa=1, $fs=0.5, center=true);
}

//Wall made out of conductive filament. Not used for recognition, only the fragile characteristic is used.
module condWall() {
    color("red")
    translate([-12.75,0,2])
    cube([0.5,12,20], center=true);
}

//Conductive part on the inside, connected to the touch screen
module condFloor() {
    color("red")
    translate([-16,0,-10.5])
    cube([4,14,1], center=true);
}

//Conductive part on the bottom, connected to the touch screen
module condTouchscreen() {
    color("red")
    translate([-16,0,-14])
    cylinder(r=sizeScreen/2,h=6, $fa=1, $fs=0.5, center=true);
}

//Conductive part on the inside, connected to the finger
module condSide() {
    color("red")
    translate([-25.5,0,-7])
    cube([1,12,4], center=true);
}

//Conducitve part on the outside, connected to the finger
module condFinger() {
    color("red")
    translate([-28,0,0])
    rotate([0,90,0])
    cylinder(r=sizeFinger/2,h=2, $fa=1, $fs=0.5, center=true);
}

//Connects the inner and outer part for the finger connection
module condConnection() {
    color("red")
    translate([-26.5,0,-4])
    cube([2,4,10], center=true);
}

//All conducitve parts in one module
module conductive() {
    union() {
        condWall();
        condFloor();
        condTouchscreen();
        condSide();
        condFinger();
        condConnection();
    }
}

//The whole object
module completeObject() {
    difference() {
        union() {
            baseBlock();
            recognitionAddition();
        }
        union() {
            waterChamber();
            frozenChamber();
            ceilingFrozen();
            ceiling();
            if (fillLater)
                fillInHole();
            conductive();
        }
    }
}

//Render the object depending on the input
if (crossSection) {
    difference() {
        if (conductive) {
            conductive();
        }
        else {
            completeObject();
        }
        translate([-30,-60,-30])
        cube([60,60,60]);
    }
}
else {
    if (conductive) {
        conductive();
    }
    else {
        completeObject();
    }
}