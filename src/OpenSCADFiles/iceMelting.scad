//This object changes its state when its temperature is above 0°C for some minutes.
//The ice will melt and flow into the other chamber where it can be detected.
//The temperature is changeable depending on what the ice is made of. For example saturated salt water will melt at ~21°C and can be detected through this method.

//Render conductive or non-conductive parts
conductive = false;
//Render the cross section or complete object
crossSection = true;
//Add a hole to fill the object after finishing printing
fillLater = true;
//Diameter of the conductive part that will be on the touch screen
sizeScreen=4.0;
//Diameter of the conductive part that will be connected to the finger
sizeFinger=4.0;

//The main block where everything else gets cut out
module baseBlock() {
    translate([1,0,0])
    cube([42,20,30], center=true);
}

//Additional part on the side wall for easier recognition which object this is
module recognitionAddition() {
    difference (){
        color("green")
        translate([-21,0,5])
        cube([2,10,40], center=true);
        baseBlock();
        roofAddition();
   }
}

//The chamber where the melted water will be caught
module conductiveChamber() {
    color("blue")
    translate([10,0,8])
    cube([16,16,26], center=true);
}

//The chamber where the ice will be filled in
module iceChamber() {
    color("blue")
    translate([-10,0,13])
    cube([16,16,16], center=true);
}

//The roof of the ice chamber
module iceRoof() {
     color("blue")
    translate([-10,0,5])
    rotate([0,45,0])
    cube([11.3,16,11.3], center=true);
}

//The roof of the conductive chamber
module conductiveRoof() {
     color("blue")
    translate([10,0,-5])
    rotate([0,45,0])
    cube([11.3,16,11.3], center=true);
}

//The tunnel where the melted water flows through to get into the conductive chamber
module meltingTunnel() {
    color("blue")
    translate([-4.5,0,-0.5])
    rotate([0,100,0])
    cylinder(r=1.5,h=15, $fa=1, $fs=0.5, center=true);
}

//Addition to the main block, added later for the roof cutout. TODO move to baseBlock
module roofAddition() {
    translate([1,0,20])
    cube([42,20,10], center=true);
}

//The hole through which the water can be filled in later
module fillInHole() {
    color("blue")
    translate([-10,0,23])
    cylinder(r=1,h=4, $fa=1, $fs=0.5, center=true);
}

//Conductive bar on the floor, connected to the touch screen
module condBarFloor() {
    color("red")
    translate([4,0,22])
    cube([4,14,2], center=true);
}

//Conductive bar on the wall, connected to the finger
module condBarWall() {
    color("red")
    translate([19,0,13])
    cube([2,14,14], center=true);
}

//Conductive part on the outside, connected to the touchscreen
module condTouchscreen() {
    color("red")
    translate([4,0,24])
    cylinder(r=sizeScreen/2,h=2, $fa=1, $fs=0.5, center=true);
}

//Conductive part on the outside, connected to the finger
module condFinger() {
    color("red")
    translate([21,0,8])
    rotate([0,90,0])
    cylinder(r=sizeFinger/2,h=2, $fa=1, $fs=0.5, center=true);
}

//All conductive parts combined to one module
module conductive() {
    union() {
        condBarFloor();
        condBarWall();
        condTouchscreen();
        condFinger();
    }
}

//The complete non-conducitve object
module completeObject() {
    difference() {
         union() {
            baseBlock();
            roofAddition();
             recognitionAddition();
        }
        union() {
            conductiveChamber();
            iceChamber();
            iceRoof();
            conductiveRoof();
            meltingTunnel();
            if (fillLater)
                fillInHole();
            conductive();
        }
    }
}

//Rotate the object for easier printing
rotate([0,180,0])

//Render the object depending on the input
if (crossSection) {
    difference() {
        if (conductive) {
        conductive();
    }
    else {
        completeObject();
    }
    translate([-30,-60,-30]) 
    cube([60,60,60]);
    }
}
else {
    if (conductive) {
        conductive();
    }
    else {
        completeObject();
    }
}