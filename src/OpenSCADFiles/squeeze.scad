//This object changes its state when the bottom half gets squeezed.
//Water will be squeezed from the bottom chamber into the top one where it can be detected.


//Render conductive or non-conductive parts
conductive = false;
//Render the cross section or complete object
crossSection = true;
//Add a hole to fill the object after finishing printing
fillLater = false;
//Diameter of the conductive part that will be on the touch screen
sizeScreen=4.0;
//Diameter of the conductive part that will be connected to the finger
sizeFinger=4.0;

//Bottom cylinder
module bottomCylinder() {
    translate([0,0,-10])
    cylinder(r=13,h=20, $fa=1, $fs=0.5, center=true);
}

//Bottom cut out cylinder where the water will be filled in
module bottomCutout() {
    color("blue")
    translate([0,0,-9])
    cylinder(r=8,h=18, $fa=1, $fs=0.5, center=true);
}

//Top cylinder
module topCylinder() {
    translate([0,0,10])
    cylinder(r=13,h=20, $fa=1, $fs=0.5, center=true);
}

//Top cot out where the water will be caught
module topCutout() {
    color("blue")
    translate([0,0,5])
    cylinder(r=8,h=10, $fa=1, $fs=0.5, center=true);
}

//The cone for the roof, used for easier printing
module topCone() {
   color("blue")
    translate([0,0,14.5])
    cylinder(r1=8, r2=0,h=9, $fa=1, $fs=0.5, center=true); 
}

//The hole through which the water can be filled in later
module fillInHole() {
    color("blue")
    translate([0,0,19])
    cylinder(r=1,h=2, $fa=1, $fs=0.5, center=true);
}

//Outer part of the cone
module coneOutside() {
   translate([0,0,5])
    cylinder(10,10,2, $fa=1, $fs=0.5, center=true); 
}

//Inner cut out part of the cone
module coneCutout() {
   translate([0,0,5])
    cylinder(10,8,1, $fa=1, $fs=0.5, center=true); 
}

//The cone where the water gets squeezed through, the water will be caught on its top
module coneMid() {
    difference() {
        coneOutside();
        coneCutout();
    }
}

//Isolation around the conductive bottom part
module isolationBottom() {
    difference() {
        color("blue")
        translate([0,-12-sizeScreen/2,-6])
        cube([4+sizeScreen,4+sizeScreen,28], center=true);
        topCylinder();
        bottomCylinder();
    }
}

//Isolation around the conductive top part
module isolationTop() {
    difference() {
        color("blue")
        translate([0,12+sizeFinger/2,11])
        cube([sizeFinger+4,sizeFinger+4,18], center=true);
        topCylinder();
        bottomCylinder();
    }
}

//Bevel for the top isolation, for easier printing and recognizing what the top part is
module isolationTopBevel() {
   difference() {
        color("blue")
        translate([0,9.9+sizeFinger/2,5-sizeFinger/2])
       rotate([45,0,0])
        cube([sizeFinger+4,1+sizeFinger*1.5,10], center=true);
        topCylinder();
        bottomCylinder();
    } 
}

//Conductive bar on the top part, connected to the finger
module condTop() {
    color("red")
        translate([0,12+sizeFinger/2,11])
        cube([sizeFinger,sizeFinger,18], center=true);
}

//Conductive part that connects the bar to the inside
module condTopInside() {
    difference() {
    color("red")
        translate([0,10,4])
        cube([4,7,3], center=true);
        topCutout();
    }
}

//Conductive bar on the bottom part, connected to the touchscreen
module condBottom() {
    color("red")
        translate([0,-12-sizeScreen/2,-7])
        cube([sizeScreen,sizeScreen,26], center=true);
}

//Conductive part that connects the bar to the inside
module condBottomInside() {
    difference() {
    color("red")
        translate([0,-10,4])
        cube([4,7,3], center=true);
        topCutout();
        coneMid();
    }
}

//All conductive parts combined to one module
module conductive() {
    union() {
        condTop();
        condTopInside();
        condBottom();
        condBottomInside();
    }
}

//The combined, non-conductive bottom part
module combinedBottom() {
    difference() {
        bottomCylinder();   
        union() {
            bottomCutout();
            conductive();
        }
    }
}

//The combined, non-conductive top part
module combinedTop() {
    union() {
        coneMid();
        difference() {
            union() {
               topCylinder(); 
               isolationBottom();
               isolationTop();
               isolationTopBevel();
            }  
            union() {
                conductive();
                topCone();
                topCutout();
                if (fillLater)
                    fillInHole();
            }
        }
    }
}

//Combined top and bottom parts to one module.
//If a printer can choose different densities for different parts, you can print the top part with 100% infill (solid) and the bottom part with ~20% (flexible) 
module completeObject() {
    union() {
        combinedBottom();
        combinedTop();
    }
}

//Render the object depending on the input
if (crossSection) {
    difference() {
       if (conductive) {
           conductive();
       }
       else {
           completeObject();
       }
       translate([0,-30,-30])
       cube([60,60,60]) ;
    }
}
else {
    if (conductive) {
        conductive();
    }
    else {
        completeObject();
    }
}