//Render conductive or non-conductive parts
conductive = false;
//Render the cross section or complete object
crossSection = true;
//Add a hole to fill the object after finishing printing
fillLater = false;
//Diameter of the conductive part that will be on the touch screen
sizeScreen=4.0;
//Diameter of the conductive part that will be connected to the finger
sizeFinger=4.0;

//Base block where everything else gets cut out
module baseBlock() {
    translate([0,0,0])
    cube([30,18,40], center=true);
}

//Chamber that will be filled with water
module waterChamber() {
    color("blue")
    translate([-6,0,-2])
    cylinder(r=7.5,h=30, $fa=1, $fs=0.5, center=true);
}

//Roof of the water chamber
module waterRoof() {
    color("blue")
    translate([-6,0,12])
    sphere(r=7.5, center=true);
}

//Hole to fill water into the chamber
module waterHole() {
    color("blue")
    translate([-6,0,18])
    cylinder(r=1,h=6, $fa=1, $fs=0.5, center=true);
}

//Connection between the two chambers
module tunnel() {
    color("blue")
    translate([])
    rotate([0,90,0])
    cylinder(r=3,h=10, $fa=1, $fs=0.5, center=true);
}

//Second chamber that catches the water
module catchChamber() {
    color("blue")
    translate([8,0,0])
    cylinder(r=5,h=30, $fa=1, $fs=0.5, center=true);
}

//Roof of the second chamber
module roofCatch() {
    color("blue")
    translate([8,0,14])
    sphere(r=5, center=true);
}

//Hole where the air gets pushed through
module holeCatch() {
    color("blue")
    translate([8,0,18])
    cylinder(r=1,h=6, $fa=1, $fs=0.5, center=true);
}

//Conductive part on the inside bottom
module condFloor() {
    color("red")
    translate([8,0,-16])
    cylinder(r=5,h=2, $fa=1, $fs=0.5, center=true);
}

//Touchscreen electrode
module condTouchscreen() {
    color("red")
    translate([8,0,-18.5])
    cylinder(r=sizeScreen/2,h=3, $fa=1, $fs=0.5, center=true);
}

//Finger electrode
module condFinger() {
    color("red")
    translate([13,0,-10])
    rotate([0,90,0])
    cylinder(r=sizeFinger/2,h=4, $fa=1, $fs=0.5, center=true);
}

//Finger electrode cut to match the cylinder shape
module condFingerCut() {
    difference() {
        condFinger();
         catchChamber();
    }
}

module conductive() {
    difference() {
        union() {
            condFloor();
            condTouchscreen();
            condFingerCut();
        }
         catchChamber();
    }
}

module completeObject() {
    difference() {
        baseBlock();
        union() {
            waterChamber();
            waterRoof();
            waterHole();
            tunnel();
            catchChamber();
            roofCatch();
            holeCatch();
            conductive();
        }
    }    
}

//Render the object depending on the input
if (crossSection) {
    difference() {
        if (conductive) {
            conductive();
        }
        else {
            completeObject();
        }
        translate([-30,-60,-30])
        cube([60,60,60]);
    }
}
else {
    if (conductive) {
        conductive();
    }
    else {
        completeObject();
    }
}