//Render conductive or non-conductive parts
conductive = false;
//Render the cross section or complete object
crossSection = false;
//Add a hole to fill the object after finishing printing
fillLater = true;
//Diameter of the conductive part that will be on the touch screen
sizeScreen=4.0;
//Diameter of the conductive part that will be connected to the finger
sizeFinger=4.0;
//Number of supports between the plates
numberSupports=2;

//Calculate for the for-loop
a=numberSupports/2 - 1;

//First plate
module plateOne() {
    cube([61,20,4], center=true);
}

//Second plate
module plateTwo() {
    translate([0,0,10])
    cube([61,20,4], center=true);
}

//Supports between the plates, number dependend on input
module supports() { 
    color("red")
   for (i = [0:1:a]) {
      translate([-26+i*8,0,5])
       rotate([0,45,0])
       cube([0.5,20,12], center=true);
   } 
   
   color("red")
   for (i = [0:1:a]) {
       translate([26-i*8,0,5])
       rotate([0,45,0])
       cube([0.5,20,12], center=true);
   }
}

//Larger bottom area for better printing
module printSupportOne() {
    translate([0,-9.75,-3])
    rotate([90,0,0])
    cube([61,10,0.5], center=true);
}

module printSupportTwo() {
    translate([0,-9.75,13])
    rotate([90,0,0])
    cube([61,10,0.5], center=true);
}

//Conductive bars between the plates
module condBar1() {
    color("red")
    translate([1,0,2.25])
    cube([4,20,2],center=true);
}

module condBar2() {
    color("red")
    translate([-1,0,7.75])
    cube([4,20,2],center=true);
}

//Conductive pins on the outside, connected to the finger or touchscreen
module condPin1() {
    color("red")
    translate([1,0,-0.05])
    cylinder(r=sizeFinger/2,h=4.1, $fa=1, $fs=0.5, center=true);
}

module condPin2() {
    color("red")
    translate([-1,0,10.05])
    cylinder(r=sizeScreen/2,h=4.1, $fa=1, $fs=0.5, center=true);
}

//All conductive parts in one module
module conductive() {
    union() {
        condBar1();
        condBar2();
        condPin1();
        condPin2();
        supports();
    }
}

//All non-conducitve parts in one module
module completeObject() {
    rotate([90,0,0])
    difference() {
        union() {
            plateOne();
            plateTwo();
            printSupportOne();
            printSupportTwo();
        }
        conductive();
    }
}

//Render dependend on inputs
if (crossSection) {
    difference() {
        if(conductive) {
            rotate([90,0,0])
            conductive();
        }
        else {
            completeObject();
        }
        translate([-40,-30,-60])
        cube([80,60,60]);
        
    }
}
else {
    if(conductive) {
        rotate([90,0,0])
        conductive();
    }
    else {
        completeObject();
    }
}
