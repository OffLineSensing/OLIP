package controller;

import java.nio.file.Path;

import model.Settings;
import model.Settings.Temp;
import model.Settings.Tilt;

/**
 * Controller class that connects the view with the model.
 * @author Martin Herbers
 *
 */
public class Controller {

	private static Settings settings = Settings.getSettings();
	
	private static Controller controller = new Controller();
	
	
	
	public static Controller getC() {
		return controller;
	}
	
	
	public void acceleration(boolean enable) {
		if (enable)
      		settings.addAcceleration();
      	else
      		settings.removeAcceleration();
	}
	
	public void setAcceleration(double a) {
		settings.setAcceleration(a);
	}
	
	public void weight(boolean enable) {
		if (enable)
      		settings.addWeight();
      	else
      		settings.removeWeight();
	}
	
	public void setWeight(int w) {
		settings.setWeight(w);
	}
	
	public void temperature(boolean enable) {
		if (enable)
      		settings.addTemperature();
      	else
      		settings.removeTemperature();
	}
	
	public void setTemperature(Temp t) {
		settings.setTemperature(t);
	}
	
	public void tilt(boolean enable) {
		if (enable)
      		settings.addTilt();
      	else
      		settings.removeTilt();
	}
	
	public void setTilt(Tilt t) {
		settings.setTilting(t);
	}
	
	public void squeeze(boolean enable) {
		if (enable)
      		settings.addSqueeze();
      	else
      		settings.removeSqueeze();
	}


	public void setFill(boolean selected) {
		settings.setFill(selected);
	}
	
	public boolean getFill() {
		return settings.getFill();
	}

	public void setSizeScreen(float f) {
		settings.setSizeScreen(f);
	}
	
	public float getSizeScreen() {
		return settings.getSizeScreen();
	}
	
	public void setSizeFinger(float f) {
		settings.setSizeFinger(f);
	}
	
	public float getSizeFinger() {
		return settings.getSizeFinger();
	}
	
	public void setOpenSCADPath(Path p) {
		settings.setOpenSCADPath(p);
	}

	public Path getOpenSCADPath() {
		return settings.getOpenSCADPath();
	}
	
	//ADD CODE HERE FOR ADDITIONAL INTERACTION
	//Only connect model and view, similar to those above
	
}
