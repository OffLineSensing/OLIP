package model;

import java.util.Arrays;
import java.util.List;

/**
 * Lists instructions for each object in its own function. Those are functions because otherwise it would only check the first time which string it should take (fill later or not)
 * @author Martin Herbers
 *
 */
public class Instructions {

	public static List<String> getSqueezeInstruction() {
		List<String> squeeze = Arrays.asList( 
			 "1. \"output.stl\" contains the main object, \"output_conductive.stl\" contains the object's conductive parts. Put \"objectSpecs.txt\" into the Android app.",
			 "2. Import output.stl into your slicing software and set it's extruder to the one extruding flexible filament.",
			 "3. Add output_conductive.stl as a part of your object and set it's extruder to the one extruding conductive filament.",
			 "4. Print.",
			 "NOTE: It can be difficult to get the flexible part to be leakproof. Try printing at a lower speed and/or using more filament (e.g. Cura: Flow 125%)",
			 Settings.getSettings().getFill()?
					 "5. When finished printing fill around 5ml of tap water with, for example, a syringe through the whole into the lower chamber. The needle needs to be straight downwards, otherwise the water will be in the wrong chamber.":
					 "5. Pause the print at some point and fill around 5ml of tap water with, for example, a syringe through the cone into the lower chamber until it is full.",
		 	 "6. If you want to check the object's current state, load the objectSpecs.txt into the Android app and place the object with the bottom on the screen.",
		 	 "NOTE: If you're unsure which side is the top and wich one is the bottom: The conductive part with a bevel is the top one.",
		 	 "NOTE: If you want to test whether no water got into the wrong chamber, test this object with the app before you start the interaction."
		 	 );
	
		return squeeze;
	}
	
	public static List<String> getWeightInstruction() {
		List<String> weight = Arrays.asList( 
			 "1. \"output.stl\" contains the main object, \"output_conductive.stl\" contains the object's conductive parts. Put \"objectSpecs.txt\" into the Android app.",
			 "2. Import output.stl into your slicing software and set it's extruder to the one extruding non-conductive filament.",
			 "3. Add output_conductive.stl as a part of your object and set it's extruder to the one extruding conductive filament.",
			 "4. Print.",
			 "5. When finished printing cut off the print support area with e.g. scissors.",
			 "6. During the test phase, the weight has to come from one flat side of the plate.",
			 "7. If you want to check the object's current state, load the objectSpecs.txt into the Android app, place one plate on the touchscreen (ideally the one where the conductive part stands out more) and put your finger on the top plate's conductive part. You can use some pressure to counteract the support's spring effect.",
			 "NOTE: If you want to test whether the conductive bars already have a connection, test this object wit hthe app before you start the interaction."
			 );
	
		return weight;
	}
			
	public static List<String> getTemperatureMeltInstruction() {
		List<String> temperatureMelt = Arrays.asList( 
			 "1. \"output.stl\" contains the main object, \"output_conductive.stl\" contains the object's conductive parts. Put \"objectSpecs.txt\" into the Android app.",
			 "2. Import output.stl into your slicing software and set it's extruder to the one extruding non-conductive filament.",
			 "3. Add output_conductive.stl as a part of your object and set it's extruder to the one extruding conductive filament.",
			 "4. Print.",
			 Settings.getSettings().getFill()?
					"5. When the print is finished, place the object on the side that is standing out, so that the side with only the conductive point is on top and the hole is on the side. Now fill some water through that hole with e.g. a syringe and freeze it either in the same position without tilting it or with the hole on the bottom. It can be helpful to seal the hole with a bit of glue.":
					"5. Before the chamber without conductive parts gets closed, pause the print and fill in some ice or water. When you're done filling resume the print. If you chose water, freeze it with the same orientation.",
			 "6. When the water is frozen, flip the object so the conductive point is on the top side. In this position the molten water will flow into the other chamber.",
			 "7. If you want to check the current state of the object, load the objectSpecs.txt into the Android app and flip the object again (conductive part on the bottom). Let the object warm up for at least 30 minutes in this position. After that the state can be checked with the Android app.",
			 "NOTE: If you're unsure which side with a conductive point is the top/bottom one and which is the one on the side, check for the printing direction. The top/bottom one is much smoother.",
			 "NOTE: If you want to test whether no water got into the second chamber, test this object with the app before you start the interaction"
			 );
		
		return temperatureMelt;
	}
	
	public static List<String> getTemperatureFreezeInstruction() {
		List<String> temperatureFreeze = Arrays.asList(
				 "1. \"output.stl\" contains the main object, \"output_conductive.stl\" contains the object's conductive parts. Put \"objectSpecs.txt\" into the Android app.",
				 "2. Import output.stl into your slicing software and set it's extruder to the one extruding non-conductive filament.",
				 "3. Add output_conductive.stl as a part of your object and set it's extruder to the one extruding conductive filament.",
				 "4. Print.",
				 Settings.getSettings().getFill()?
						"5. When the print is finished, fill around 8ml of tap water through the hole with, for example, a syringe. Ideally a bit of water comes back out. You can seal the hole with some glue.":
						"5. Before the chamber without conductive parts gets closed, pause the printing and fill it with water. The more water the better it will work.",
				 "6. When the print and filling finished, the object can be used instantly. When it freezes, the water ice will expand and break through the conductive wall. When melting again, the water will flow through the broken wall.",
				 "7. If you want to check the current state of the object, load the objectSpecs.txt into the Android app and let the object warm up for at least 30 minutes. After that the state can be checked with the Android app.",
				 "NOTE: If you want to test whether no water got into the wrong chamber (e.g. the wall isn't leakproof), test this object with the app before you start the interaction."
				);
		
		return temperatureFreeze;
	}

	public static List<String> getAccelerationInstruction() {
		List<String> acceleration = Arrays.asList( 
			 "1. \"output.stl\" contains the main object, \"output_conductive.stl\" contains the object's conductive parts. Put \"objectSpecs.txt\" into the Android app.",
			 "2. Import output.stl into your slicing software and set it's extruder to the one extruding non-conductive filament.",
			 "3. Add output_conductive.stl as a part of your object and set it's extruder to the one extruding conductive filament.",
			 "4. Print.",
			 Settings.getSettings().getFill()?
					"5. Fill 3ml  of tap water with, for example, a syringe into the part without conductive material.":
					"5. Before the ceiling gets printed, pause the print and fill 3ml of tap water into the chamber without conductive parts.",
			 "NOTE: A small derivation of the water volume can change the acceleration at which the state changes, so try to be as accurate as possible.",
			 "NOTE: Try to hold the object without tilt since a small tilt will change the acceleratoin at which the state changes.",
			 "6. During the test phase and before reading the object's state try not to tilt it. The object will keep it's state until it gets tilted.",
			 "7. If you want to check the object's current state, load the objectSpecs.txt into the Android app and place the bottom side of the object on the touchscreen and put your finger on conductive part on the side."
			 );
	
		return acceleration;	
	}
	
	public static List<String> getFlipInstruction() {
		List<String> flip =  Arrays.asList( 
			 "1. \"output.stl\" contains the main object, \"output_conductive.stl\" contains the object's conductive parts. Put \"objectSpecs.txt\" into the Android app.",
			 "2. Import output.stl into your slicing software and set it's extruder to the one extruding non-conductive filament.",
			 "3. Add output_conductive.stl as a part of your object and set it's extruder to the one extruding conductive filament.",
			 "4. Print.",
			 Settings.getSettings().getFill()?
					 "5. After the print finished fill around 4ml of tap water with, for example, a syringe through the hole into the middle part. Try holding both the object and the syringe straight upwards so no water can accidently get into the outer chamber. You can seal the hole with some glue.":
					 "5. Before the ceiling gets printed, pause the print and fill 4ml of tap water into the middle chamber.",
			 "6. During the test phase, small tilts won't change the state. Only around 90-270� tilts will change the state.",
			 "7. If you want to check the current state of the object, load the objectSpecs.txt into the Android app and place the object on the screen. Put your finger on the conductive part on the side.",
			 "NOTE: If you want to test whether no water got into the wrong chamber, test this object with the app before you start the interaction"
			 );
	
		return flip;
	}
	
	public static List<String> getTiltInstruction() {
		List<String> tilt =  Arrays.asList( 
			 "1. \"output.stl\" contains the main object, \"output_conductive.stl\" contains the object's conductive parts. Put \"objectSpecs.txt\" into the Android app.",
			 "2. Import output.stl into your slicing software and set it's extruder to the one extruding non-conductive filament.",
			 "3. Add output_conductive.stl as a part of your object and set it's extruder to the one extruding conductive filament.",
			 "4. Print.",
			 Settings.getSettings().getFill()?
					 "5. After the print finished fill around 2ml of tap water with, for example, a syringe through the hole into the part further away from the electrodes.":
					 "5. Before the ceiling gets printed, pause the print and fill 2ml of tap water into the chamber without conductive parts.",
			 "6. During the test phase, small tilts won't change the state. Only around 45-135� tilts will change the state.",
			 "7. If you want to check the current state of the object, load the objectSpecs.txt into the Android app and place the object on the screen. Put your finger on the conductive part on the side.",
			 "NOTE: If you want to test whether no water got into the wrong chamber, test this object with the app before you start the interaction"
			 );
	
		return tilt;
	}
	
	//ADD CODE HERE FOR ADDITIONAL INTERACTION
	//New function that returns the textual instructions as a List of Strings
}
