package model;

import java.nio.file.Path;

/**
 * This class saves the current settings. Those are needed for creating the STLs.
 * @author Martin Herbers
 *
 */
public class Settings {
	public enum Temp {
		UNDER0, OVER0
	}
	
	public enum Tilt {
		TILT, FLIP
	}

	//The 5 interactions that can currently be detected.
	private boolean weight = false;
	private boolean acceleration = false;
	private boolean squeeze = false;
	private boolean tilt = false;
	private boolean temperature = false;
	//ADD CODE HERE FOR ADDITIONAL INTERACTION
	//New boolean for each interaction, allows combining them later
	
	//Some more settings to create more interaction possibilities
	private int weightSetting = 54;
	private double accelerationSetting = 5;
	private Temp temperatureSetting = Temp.OVER0;
	private Tilt tiltSetting = Tilt.TILT;
	//ADD CODE HERE FOR ADDITIONAL INTERACTION
	//Only if additional data is necessary
	
	//Settings from the settings window that can be used for multiple objects
	private boolean fill = false;
	private float sizeScreen = 4.0f;
	private float sizeFinger = 4.0f;
	private Path openScadPath = null;
	
	private static Settings settings = new Settings();
	
	public static Settings getSettings() {
		return settings;
	}
	
	public void addWeight() {
		weight = true;
	}
	
	public void removeWeight() {
		weight = false;
	}
	
	public boolean isWeight() {
		return weight;
	}
	
	public void addAcceleration() {
		acceleration = true;
	}
	
	public void removeAcceleration() {
		acceleration = false;
	}
	
	public boolean isAcceleration() {
		return acceleration;
	}
	
	public void addSqueeze() {
		squeeze = true;
	}
	
	public void removeSqueeze() {
		squeeze = false;
	}
	
	public boolean isSqueeze() {
		return squeeze;
	}
	
	public void addTilt() {
		tilt = true;
	}
	
	public void removeTilt() {
		tilt = false;
	}
	
	public boolean isTilt() {
		return tilt;
	}
	
	public void addTemperature() {
		temperature = true;
	}
	
	public void removeTemperature() {
		temperature = false;
	}
	
	public boolean isTemperature() {
		return temperature;
	}
	
	public void setWeight(int w) {
		weightSetting = w;
	}
	
	public int getWeight() {
		return weightSetting;
	}
	
	public void setAcceleration(double a) {
		accelerationSetting = a;
	}
	
	public double getAcceleration() {
		return accelerationSetting;
	}
	
	public void setTemperature(Temp t) {
		temperatureSetting = t;
	}
	
	public Temp getTemperature() {
		return temperatureSetting;
	}
	
	public void setTilting (Tilt t) {
		tiltSetting = t;
	}
	
	public Tilt getTilting() {
		return tiltSetting;
	}
	
	public void setFill(boolean b) {
		fill = b;
	}
	
	public boolean getFill() {
		return fill;
	}
	
	public void setSizeScreen(float f) {
		sizeScreen = f;
	}
	
	public float getSizeScreen() {
		return sizeScreen;
	}
	
	public void setSizeFinger(float f) {
		sizeFinger = f;
	}
	
	public float getSizeFinger() {
		return sizeFinger;
	}
	
	public void setOpenSCADPath(Path p) {
		openScadPath = p;
	}
	
	public Path getOpenSCADPath() {
		return openScadPath;
	}
	
	//ADD CODE HERE FOR ADDITIONAL INTERACTION
	//At least get/set for the boolean, possibly also for additional data like size
	
}

