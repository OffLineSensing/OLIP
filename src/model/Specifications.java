package model;

import java.util.Arrays;
import java.util.List;


/**
 * Lists the specification. Used for the Android app. Template:
 * Name:[Object name, shown on top of the app]
 * Shape:[object's shape and position][...]...
 * Contact:[shape and position of the conductive part]
 * ButtonTrue:[message that appears below the object on the app if the object did change its state]
 * ButtonFalse:[initial message that stays if the object didn't change its state]
 * 
 * Shape information: 	rectangle: [R; x position from the object's middle; y position; width; height;]
 * 						circle: [C; x position; y position; diameter]
 * 						position and size in millimeter. E.g.: [R;5;5;20;20]
 * @author Martin Herbers
 *
 */

public class Specifications {
	public static List<String> getSqueezeSpecs() {
		return Arrays.asList(
				"Name:Pressure",
				"Shape:[C;0;0;26][R;0;" + (Settings.getSettings().getSizeScreen() + 12) + ";" + (Settings.getSettings().getSizeScreen() + 4) + ";" + (Settings.getSettings().getSizeScreen() + 4) + "]",
				"Contact:[R;0;" + (Settings.getSettings().getSizeScreen() + 12) + ";" + Settings.getSettings().getSizeScreen() + ";" + Settings.getSettings().getSizeScreen() + "]",
				"ButtonTrue:The object got squeezed!",
				"ButtonFalse:The object didn't get squeezed."
			 );
	}
	
	public static List<String> getWeightSpecs() {

		return Arrays.asList(
				"Name:Load " + (Settings.getSettings().getWeight()/10.0f) + "kg",
				"Shape:[R;0;0;20;61]",
				"Contact:[C;0;1;" + Settings.getSettings().getSizeScreen() + "]",
				"ButtonTrue:A weight of more than " + (Settings.getSettings().getWeight()/10.0f) + " Kg was applied!",
				"ButtonFalse:A weight of less than " + (Settings.getSettings().getWeight()/10.0f) + " Kg was applied."
			 );
	}
			
	public static List<String> getTemperatureMeltSpecs() {

		return Arrays.asList(
				"Name:Rising Temperature",
				"Shape:[R;0;0;20;42][R;0;-20;10;2]",
				"Contact:[C;0;3;" + Settings.getSettings().getSizeScreen() + "]",
				"ButtonTrue:The object was stored above 0�C!",
				"ButtonFalse:The object was cooled the whole time."
			);
	}
	
	public static List<String> getTemperatureFreezeSpecs() {

		return Arrays.asList(
				"Name:Falling Temperature",
				"Shape:[R;0;0;20;43][C;10;-8;6]",
				"Contact:[C;0;7;" + Settings.getSettings().getSizeScreen() + "]",
				"ButtonTrue:The object was stored below 0�C!",
				"ButtonFalse:The object was above 0�C the whole time."
				);
	}

	public static List<String> getAccelerationSpecs() {

		return Arrays.asList(
				"Name:Acceleration " + Settings.getSettings().getAcceleration() + "m/s�",
				"Shape:[R;0;0;20;40][C;0;-10;6][C;0;10;6]",
				"Contact:[C;0;10;" + Settings.getSettings().getSizeScreen() + "]",
				"ButtonTrue:The object was accelerated with " + Settings.getSettings().getAcceleration() + "m/s�!",
				"ButtonFalse:The object was accelerated with less than " + Settings.getSettings().getAcceleration() + "m/s�."
			 );
	}
	
	public static List<String> getFlipSpecs() {

		return Arrays.asList(
				"Name:Flip",
				"Shape:[C;0;0;34]",
				"Contact:[C;0;0;" + Settings.getSettings().getSizeScreen() + "]",
				"ButtonTrue:The object got flipped!",
				"ButtonFalse:The object didn't get flipped."
			 );
	}
	
	public static List<String> getTiltSpecs() {

		return Arrays.asList(
				"Name:Tilt",
				"Shape:[R;0;0;18;30]",
				"Contact:[C;0;8;" + Settings.getSettings().getSizeScreen() + "]",
				"ButtonTrue:The object got tilted!",
				"ButtonFalse:The object didn't get tilted."
			);
	}
	
	//ADD CODE HERE FOR ADDITIONAL INTERACTION
	//New function that returns the specification as listed in the top comment in a List of Stings
}
