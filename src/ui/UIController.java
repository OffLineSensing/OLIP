package ui;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class UIController  {

	private Parent root;
	private Stage stage;

	public UIController() {
		try {
			root = FXMLLoader.load(getClass().getResource("InformationWindow.fxml"));
			stage = new Stage();
			stage.setResizable(false);
			stage.setScene(new Scene(root, 450, 450));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setLegend() {

		Image imgBlue = new Image(getClass().getResource("/pictures/lightBlue.PNG").toString());	//Was blue
		ImageView imgViewBlue = new ImageView(imgBlue);
		imgViewBlue.setFitWidth(20);
		imgViewBlue.setFitHeight(20);
		((Label) root.lookup("#blueText")).setGraphic(imgViewBlue);

		Image imgRed = new Image(getClass().getResource("/pictures/red.PNG").toString());
		ImageView imgViewRed = new ImageView(imgRed);
		imgViewRed.setFitWidth(20);
		imgViewRed.setFitHeight(20);
		((Label) root.lookup("#redText")).setGraphic(imgViewRed);

		Image imgGreen = new Image(getClass().getResource("/pictures/gray.PNG").toString());	//was green
		ImageView imgViewGreen = new ImageView(imgGreen);
		imgViewGreen.setFitWidth(20);
		imgViewGreen.setFitHeight(20);
		((Label) root.lookup("#greenText")).setGraphic(imgViewGreen);

		Image imgIce = new Image(getClass().getResource("/pictures/cyan.PNG").toString());	//delete?
		ImageView imgViewIce = new ImageView(imgIce);
		imgViewIce.setFitWidth(20);
		imgViewIce.setFitHeight(20);
		((Label) root.lookup("#iceText")).setGraphic(imgViewIce);

	}

	@FXML
	protected void openSettings() {
		Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource("SettingsWindow.fxml"));

			Stage stage = new Stage();
			stage.setResizable(false);
			stage.setTitle("Settings");
			stage.setScene(new Scene(root, 450, 450));
			stage.show();
			stage.toFront();
			SettingsWindow.getWindow().setUpListener();
			SettingsWindow.getWindow().setStage(stage);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	protected void openAccelInfo() {
		stage.setTitle("Acceleration Information");
		stage.show();
		stage.setWidth(800);
		stage.setHeight(300);
		stage.toFront();
		setLegend();
		ImageView img = ((ImageView) root.lookup("#image"));
		img.setImage(new Image(getClass().getResource("/pictures/simpleAcceleration.png").toString()));
		img.setFitWidth(480);
		img.setFitHeight(350);
		((Label) root.lookup("#text1")).setText("This object changes its state when a specific acceleration is reached.\n" +
				"The specific acceleration is dependend on the wall height between the two chambers. " +
				"The acceleration can be either in the opposite direction of the small chamber or breaking after accelerating in the same direction as the small chamber.\n" +
				"Tab water needs to be filled into the right chamber. " +
				"When accelerating the object the water will swap over the wall into the small chamber where it can be detected.");

	}

	@FXML
	protected void openWeightInfo() {
		stage.setTitle("Load Information");
		stage.show();
		stage.setWidth(800);
		stage.setHeight(300);
		stage.toFront();
		setLegend();
		ImageView img = ((ImageView) root.lookup("#image"));
		img.setImage(new Image(getClass().getResource("/pictures/simpleWeight.png").toString()));
		img.setFitWidth(480);
		img.setFitHeight(350);
		((Label) root.lookup("#text1")).setText("This object changes its state when a specific load lays on it." +
				"\nThe specific load is dependend on the number of supports between the panels." +
				" When enough weight lays on this object the supports will bend, leading to less space between the panels.");

	}

	@FXML
	protected void openTemperatureUpInfo() {
		stage.setTitle("Rising Temperature Information");
		stage.show();
		stage.setWidth(800);
		stage.setHeight(300);
		stage.toFront();
		setLegend();
		ImageView img = ((ImageView) root.lookup("#image"));
		img.setImage(new Image(getClass().getResource("/pictures/simpleTemperatureMelt.png").toString()));
		img.setFitWidth(480);
		img.setFitHeight(350);
		((Label) root.lookup("#text1")).setText("This object changes its state when it is stored above 0°C for some minutes." +
				"\nTab water or ice is used for this object. You have to freeze this object to make it work." +
				" When stored above 0°C the ice starts to melt and reaches the other chamber where it can be detected. Otherwise it will stay in the first chamber." +
				" When checking for this object's state, the object will be fliped and warmed up for 30 minutes.");

	}

	@FXML
	protected void openTemperatureDownInfo() {
		stage.setTitle("Falling Temperature Information");
		stage.show();
		stage.setWidth(800);
		stage.setHeight(300);
		stage.toFront();
		setLegend();
		ImageView img = ((ImageView) root.lookup("#image"));
		img.setImage(new Image(getClass().getResource("/pictures/simpleTemperatureFreeze.png").toString()));
		img.setFitWidth(480);
		img.setFitHeight(350);
		((Label) root.lookup("#text1")).setText("This object changes its state when it is stored under 0°C for a while." +
				"\nTab water is filled into the right chamber" +
				" When stored under 0°C the water will freeze and the resulting ice will break the conductive wall." +
				" After melting, water will flow through the crack." +
				" When checking for this object's state, the object has to be placed on a touchscreen after letting it warm up for at least 30 minutes.");

	}

	@FXML
	protected void openTiltInfo() {
		stage.setTitle("Rotate by 90° Information");
		stage.show();
		stage.setWidth(800);
		stage.setHeight(350);
		stage.toFront();
		setLegend();
		ImageView img = ((ImageView) root.lookup("#image"));
		img.setImage(new Image(getClass().getResource("/pictures/simpleTilt.png").toString()));
		img.setFitWidth(480);
		img.setFitHeight(350);
		((Label) root.lookup("#text1")).setText("This object changes its state when it gets tilted by around 90° in the right direction." +
				"\nWhen tilted the water flows into the next chamber where it can be detected.");

	}

	@FXML
	protected void openFlipInfo() {
		stage.setTitle("Flip Over by 180° Information");
		stage.show();
		stage.setWidth(800);
		stage.setHeight(380);
		stage.toFront();
		setLegend();
		ImageView img = ((ImageView) root.lookup("#image"));
		img.setImage(new Image(getClass().getResource("/pictures/simpleFlip.png").toString()));
		img.setFitWidth(480);
		img.setFitHeight(350);
		((Label) root.lookup("#text1")).setText("This object changes its state when it gets fliped (rotated by around 180°)." +
				"\nThe water in the middle chamber will then flow into the outer chamber where it can be detected.");

	}

	@FXML
	protected void openSqueezeInfo() {
		stage.setTitle("Pressure Information");
		stage.show();
		stage.setWidth(800);
		stage.setHeight(350);
		stage.toFront();
		setLegend();
		ImageView img = ((ImageView) root.lookup("#image"));
		img.setImage(new Image(getClass().getResource("/pictures/simpleSqueeze.png").toString()));
		img.setFitWidth(480);
		img.setFitHeight(350);
		((Label) root.lookup("#text1")).setText("This object changes its state when the lower half gets squeezed." +
				"\nThe lower chamber has to be filled with tab water." +
				" The water will then be pressed from the lower chamber onto the cone where it then can be detected.");

	}
}
